#pragma once

#include <iostream>
#include <string>

using namespace std;

class TicTacToe
{
private:
	char m_board[9];
	// this variable is keeping track of all available spaces on the board
	int m_numTurns;
	// tells us whose turn it is and what to put into the choosen space
	char m_playerTurn;
	// this will now be used to assign if X or O win or if it ends in a draw
	char m_winner;

public:
	// constructor
	TicTacToe() 
	{
		// initializing the variables
		for (int i = 0; i < 9; i++)
		{
			m_board[i] = ' ';
		}
		m_numTurns = 0;
		m_playerTurn = 'X';
		m_winner = ' ';
	};

	// methods
	virtual void DisplayBoard()
	{
		// Create's the game title
		// Appears only at the start of the game
		if (m_numTurns == 0) cout << "\n       TIC TAC TOE		\n";
		cout << "\n";
		cout << "    1    |2    |3    \n";
		cout << "      " << m_board[0] << "  |  " << m_board[1] << "  |  " << m_board[2] << " \n";
		cout << "    _____|_____|_____\n";
		cout << "    4    |5    |6    \n";
		cout << "      " << m_board[3] << "  |  " << m_board[4] << "  |  " << m_board[5] << " \n";
		cout << "    _____|_____|_____\n";
		cout << "    7    |8    |9    \n";
		cout << "      " << m_board[6] << "  |  " << m_board[7] << "  |  " << m_board[8] << " \n";
		cout << "         |     |     \n\n";
	}

	virtual bool IsOver()
	{
		// check to see if m_winner is an empty string a.k.a. its beginning state
		if (m_winner == ' ')
		{
			// checking all possible ways a game can end & assigning m_winner with the winning state
			if (m_numTurns == 9) m_winner = 'T';

			if ((m_board[0] == m_board[1] && m_board[0] == m_board[2]) && m_board[0] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[0] == m_board[3] && m_board[0] == m_board[6]) && m_board[0] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[0] == m_board[4] && m_board[0] == m_board[8]) && m_board[0] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[1] == m_board[4] && m_board[1] == m_board[7]) && m_board[1] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[2] == m_board[5] && m_board[2] == m_board[8]) && m_board[2] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[2] == m_board[4] && m_board[2] == m_board[6]) && m_board[2] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[3] == m_board[4] && m_board[3] == m_board[5]) && m_board[3] != ' ')
			{
				m_winner = m_playerTurn;
			}
			if ((m_board[6] == m_board[7] && m_board[6] == m_board[8]) && m_board[6] != ' ')
			{
				m_winner = m_playerTurn;
			}
			return false;
		}
		else return true;
	}

	// method is being used to tell us whose turn it is
	virtual char GetPlayerTurn()
	{
		return m_playerTurn;
	}

	virtual bool IsValidMove(int position)
	{
		if (position <= 9 && position >= 1)
		{
			if (m_board[position - 1] == 'X' || m_board[position - 1] == 'O') return false;
			else return true;
		}
		else return false;
	}

	virtual void Move(int position)
	{
		m_board[position - 1] = m_playerTurn;
		// each successful move will fill up the # of spaces one can make
		m_numTurns++;
		// each turn will also check to see if it is the last move for the game
		IsOver();
		if (m_playerTurn == 'X') m_playerTurn = 'O';
		else m_playerTurn = 'X';
	}

	virtual void DisplayResult()
	{
			if (m_winner == 'T') cout << "           Tie!\n\n";
			else cout << "      Player " << m_winner << " wins!\n\n";
	}
};
